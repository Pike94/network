'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}
// search page
function pageWidget(pages) {
  var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
  widgetWrap.prependTo("body");
  for (var i = 0; i < pages.length; i++) {
    if (pages[i][0] === '#') {
      $('<li class="widget_item"><a class="widget_link" href="' + pages[i] +'">' + pages[i] + '</a></li>').appendTo('.widget_list');
    } else {
      $('<li class="widget_item"><a class="widget_link" href="' + pages[i] + '.html' + '">' + pages[i] + '</a></li>').appendTo('.widget_list');
    }
  }
  var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:fixed;top:0;left:0;z-index:9999;padding:20px 20px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_item{padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
  widgetStilization.prependTo(".widget_wrap");
}

$(document).ready(function($) {
  pageWidget(['index', 'network', 'firm', 'services', 'sertificate', 'landkarte']);
});

$(document).on('click', '.call-menu', function(){
  $('.nav').addClass('active');
});
$(document).on('click', '.close-menu', function(){
  $('.nav').removeClass('active');
});

$('.js-der-slider').slick({
  infinite: true,
  arrows: true,
  dots: false,
  prevArrow: '<button class="slick-prev slick-arrow" aria-label="Prev" type="button"></button>',
  nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button"></button>',
});

$('.js-gen-slider').slick({
  infinite: true,
  arrows: true,
  dots: false,
  prevArrow: '<button class="slick-prev slick-arrow" aria-label="Prev" type="button"></button>',
  nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button"></button>',
});

function initMap() {
  // The location of Uluru
  var uluru = {lat: 51.255698, lng: 7.149353};
  // The map, centered at Uluru
  var map = new google.maps.Map(
    document.getElementById('map'), {
      zoom: 13, 
      center: uluru,
      styles: [
        {
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#f5f5f5"
        }
        ]
        },
        {
        "elementType": "labels.icon",
        "stylers": [
        {
        "visibility": "off"
        }
        ]
        },
        {
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#616161"
        }
        ]
        },
        {
        "elementType": "labels.text.stroke",
        "stylers": [
        {
        "color": "#f5f5f5"
        }
        ]
        },
        {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#bdbdbd"
        }
        ]
        },
        {
        "featureType": "landscape",
        "stylers": [
        {
        "color": "#D6E0DE"
        }
        ]
        },
        {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#eeeeee"
        }
        ]
        },
        {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#e5e5e5"
        }
        ]
        },
        {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#9e9e9e"
        }
        ]
        },
        {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#ffffff"
        }
        ]
        },
        {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#757575"
        }
        ]
        },
        {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#dadada"
        }
        ]
        },
        {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#616161"
        }
        ]
        },
        {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#9e9e9e"
        }
        ]
        },
        {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#e5e5e5"
        }
        ]
        },
        {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#eeeeee"
        }
        ]
        },
        {
        "featureType": "water",
        "stylers": [
        {
        "color": "#C8DBE4"
        }
        ]
        },
        {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#c9c9c9"
        }
        ]
        },
        {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#9e9e9e"
        }
        ]
        }
        ]
    });
}

if($('#map').length != 0) {
  initMap();
}

function initMainMap() {
  // The location of Uluru
  var uluru = {lat: 51.255698, lng: 7.149353};

  var coords = [
    {
      lat: 51.255698, 
      lng: 7.149353
    },
    {
      lat: 51.267569, 
      lng: 7.105043
    },
    {
      lat: 51.235802, 
      lng: 7.085880
    },
  ]

  // The map, centered at Uluru
  var mainMap = new google.maps.Map(
    document.getElementById('main-map'), {
      zoom: 13, 
      center: uluru,
      styles: [
        {
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#f5f5f5"
        }
        ]
        },
        {
        "elementType": "labels.icon",
        "stylers": [
        {
        "visibility": "off"
        }
        ]
        },
        {
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#616161"
        }
        ]
        },
        {
        "elementType": "labels.text.stroke",
        "stylers": [
        {
        "color": "#f5f5f5"
        }
        ]
        },
        {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#bdbdbd"
        }
        ]
        },
        {
        "featureType": "landscape",
        "stylers": [
        {
        "color": "#D6E0DE"
        }
        ]
        },
        {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#eeeeee"
        }
        ]
        },
        {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#e5e5e5"
        }
        ]
        },
        {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#9e9e9e"
        }
        ]
        },
        {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#ffffff"
        }
        ]
        },
        {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#757575"
        }
        ]
        },
        {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#dadada"
        }
        ]
        },
        {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#616161"
        }
        ]
        },
        {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#9e9e9e"
        }
        ]
        },
        {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#e5e5e5"
        }
        ]
        },
        {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#eeeeee"
        }
        ]
        },
        {
        "featureType": "water",
        "stylers": [
        {
        "color": "#C8DBE4"
        }
        ]
        },
        {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#c9c9c9"
        }
        ]
        },
        {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#9e9e9e"
        }
        ]
        }
        ]
    });
    var markers = []
    var marker, i;
    for(i = 0; i < coords.length; i++){
      marker = new google.maps.Marker({
        position: coords[i],
        map: mainMap,
        icon: {
          url: "assets/img/svg/pin.svg",
        },
      });
      
      markers.push(marker);
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        $(document).on('click', '.map-modal-close', function(){
          marker.setIcon("assets/img/svg/pin.svg");
          $(this).parents('.map-modal').fadeOut();
        });
        return function(){
          for (var j = 0; j < markers.length; j++) {
            markers[j].setIcon("assets/img/svg/pin.svg");
          }
          marker.setIcon("assets/img/svg/pin-active.svg");
          $('.map-modal').fadeOut();
          $('#' + i + '.map-modal').fadeIn();
        }
      })(marker, i));
    };
}

if($('#main-map').length != 0) {
  initMainMap();
}

$(document).on('click', '.show-more-btn', function(){
  $(this).siblings('.hide-block').toggleClass('active');
});

$(document).on('click', '.show-more-desc', function(){
  $(this).siblings('.dedsc-hide').toggleClass('active');
});